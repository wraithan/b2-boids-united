use std::fs::read_to_string;
use std::path::Path;

use anyhow::{Context, Result};
use rendy::shader::{ShaderKind, SourceCodeShaderInfo, SourceLanguage, SpirvShader};

pub fn load_and_compile_shader(location: &Path, kind: ShaderKind) -> Result<SpirvShader> {
    let shader = read_to_string(location)
        .with_context(|| format!("Couldn't read shader file: {:?}", location))?;

    SourceCodeShaderInfo::new(
        &shader,
        location
            .to_str()
            .with_context(|| format!("location not a str? {:?}", location))?,
        kind,
        SourceLanguage::GLSL,
        "main",
    )
    .precompile()
    .with_context(|| format!("Failed to compile shader: {:?}", location))
}
