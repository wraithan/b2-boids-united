mod render;
mod settings;

use rendy::{
    factory::Config as RendyConfig,
    init::{
        winit::{
            dpi::LogicalSize,
            event::{Event, WindowEvent},
            event_loop::{ControlFlow, EventLoop},
            window::WindowBuilder,
        },
        AnyWindowedRendy,
    },
};

use render::build_graph;

fn main() {
    pretty_env_logger::init();
    let settings = settings::load_settings().expect("Couldn't load settings");
    let rendy_config: RendyConfig = Default::default();
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_inner_size(LogicalSize::new(960, 640))
        .with_title("Rendy example");

    let rendy = AnyWindowedRendy::init_auto(&rendy_config, window, &event_loop).unwrap();
    rendy::with_any_windowed_rendy!((rendy)
        (mut factory, mut families, surface, window) => {
            let mut graph = Some(build_graph(&mut factory, &mut families, surface, &window, &settings).expect("Couldn't build graph"));

            let started = std::time::Instant::now();

            let mut frame = 0u64;
            let mut elapsed = started.elapsed();

            event_loop.run(move |event, _, control_flow| {
                *control_flow = ControlFlow::Poll;
                match event {
                    Event::WindowEvent { event, .. } => match event {
                        WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                        WindowEvent::Resized(_dims) => {
                            let started = std::time::Instant::now();
                            graph.take().unwrap().dispose(&mut factory, &settings);
                            log::trace!("Graph disposed in: {:?}", started.elapsed());
                            return;
                        }
                        _ => {}
                    },
                    Event::MainEventsCleared => {
                        factory.maintain(&mut families);
                        if let Some(ref mut graph) = graph {
                            graph.run(&mut factory, &mut families, &settings);
                            frame += 1;
                        }

                        elapsed = started.elapsed();
                        if elapsed >= std::time::Duration::new(5, 0) {
                            *control_flow = ControlFlow::Exit
                        }
                    }
                    _ => {}
                }

                if *control_flow == ControlFlow::Exit && graph.is_some() {
                    let elapsed_ns = elapsed.as_secs() * 1_000_000_000 + elapsed.subsec_nanos() as u64;

                    log::info!(
                        "Elapsed: {:?}. Frames: {}. FPS: {}",
                        elapsed,
                        frame,
                        frame * 1_000_000_000 / elapsed_ns
                    );

                    graph.take().unwrap().dispose(&mut factory, &settings);
                }
            });
        }
    );
}
