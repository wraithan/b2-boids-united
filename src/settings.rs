use std::fs;
use std::path::{Path, PathBuf};

use anyhow::{Context, Result};
use serde::{Deserialize, Serialize};
use toml;

#[derive(Deserialize, Serialize)]
pub struct Settings {
    pub shaders: ShaderSettings,
}

#[derive(Deserialize, Serialize)]
pub struct ShaderSettings {
    frag_path: PathBuf,
    vert_path: PathBuf,
    comp_path: PathBuf,
}

pub trait SettingsAccessors {
    fn get_frag(&self) -> &Path;
    fn get_vert(&self) -> &Path;
    fn get_comp(&self) -> &Path;
}

impl SettingsAccessors for Settings {
    fn get_frag(&self) -> &Path {
        &self.shaders.frag_path
    }

    fn get_vert(&self) -> &Path {
        &self.shaders.vert_path
    }

    fn get_comp(&self) -> &Path {
        &self.shaders.comp_path
    }
}

pub fn load_settings() -> Result<Settings> {
    let settings_path = concat!(env!("CARGO_MANIFEST_DIR"), "/settings.toml");
    log::info!("settings_path: {}", settings_path);
    let settings =
        fs::read_to_string(settings_path).with_context(|| "couldn't read settings file")?;
    toml::from_str(&settings).with_context(|| "couldn't parse settings file as toml")
}
